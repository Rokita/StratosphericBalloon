EESchema Schematic File Version 2
LIBS:SchematicLib
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:MainControlUnit-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L FT230XS U6
U 1 1 5B240B42
P 5200 3750
F 0 "U6" H 4850 4400 60  0000 C CNN
F 1 "FT230XS" H 5450 4400 60  0000 C CNN
F 2 "Housings_SSOP:SSOP-16_5.3x6.2mm_Pitch0.65mm" H 5650 4550 60  0001 C CNN
F 3 "" H 5650 4550 60  0001 C CNN
F 4 "TSSOP16" H 5150 3050 60  0000 C CNN "Package"
	1    5200 3750
	1    0    0    -1  
$EndComp
$Comp
L R R20
U 1 1 5B240C2D
P 3350 3450
F 0 "R20" V 3430 3450 50  0000 C CNN
F 1 "27" V 3350 3450 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 3280 3450 50  0001 C CNN
F 3 "" H 3350 3450 50  0001 C CNN
	1    3350 3450
	0    1    1    0   
$EndComp
$Comp
L R R21
U 1 1 5B240CAA
P 3350 3650
F 0 "R21" V 3430 3650 50  0000 C CNN
F 1 "27" V 3350 3650 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 3280 3650 50  0001 C CNN
F 3 "" H 3350 3650 50  0001 C CNN
	1    3350 3650
	0    1    1    0   
$EndComp
$Comp
L C C18
U 1 1 5B240D65
P 3050 3900
F 0 "C18" H 3075 4000 50  0000 L CNN
F 1 "47p" H 3075 3800 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 3088 3750 50  0001 C CNN
F 3 "" H 3050 3900 50  0001 C CNN
	1    3050 3900
	1    0    0    -1  
$EndComp
$Comp
L C C17
U 1 1 5B240E1C
P 2800 3900
F 0 "C17" H 2825 4000 50  0000 L CNN
F 1 "47p" H 2825 3800 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2838 3750 50  0001 C CNN
F 3 "" H 2800 3900 50  0001 C CNN
	1    2800 3900
	1    0    0    -1  
$EndComp
$Comp
L C C21
U 1 1 5B240E43
P 4250 4100
F 0 "C21" H 4275 4200 50  0000 L CNN
F 1 "100n" H 4275 4000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4288 3950 50  0001 C CNN
F 3 "" H 4250 4100 50  0001 C CNN
	1    4250 4100
	1    0    0    -1  
$EndComp
$Comp
L C C16
U 1 1 5B240EA1
P 2550 3900
F 0 "C16" H 2575 4000 50  0000 L CNN
F 1 "10n" H 2575 3800 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2588 3750 50  0001 C CNN
F 3 "" H 2550 3900 50  0001 C CNN
	1    2550 3900
	1    0    0    -1  
$EndComp
$Comp
L LED D7
U 1 1 5B2411A1
P 6150 2900
F 0 "D7" H 6150 3000 50  0000 C CNN
F 1 "LED" H 6150 2800 50  0000 C CNN
F 2 "LEDs:LED_0603_HandSoldering" H 6150 2900 50  0001 C CNN
F 3 "" H 6150 2900 50  0001 C CNN
	1    6150 2900
	0    1    -1   0   
$EndComp
$Comp
L LED D8
U 1 1 5B241274
P 6500 2900
F 0 "D8" H 6500 3000 50  0000 C CNN
F 1 "LED" H 6500 2800 50  0000 C CNN
F 2 "LEDs:LED_0603_HandSoldering" H 6500 2900 50  0001 C CNN
F 3 "" H 6500 2900 50  0001 C CNN
	1    6500 2900
	0    1    -1   0   
$EndComp
$Comp
L R R22
U 1 1 5B241A7C
P 6150 3300
F 0 "R22" V 6230 3300 50  0000 C CNN
F 1 "270" V 6150 3300 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 6080 3300 50  0001 C CNN
F 3 "" H 6150 3300 50  0001 C CNN
	1    6150 3300
	-1   0    0    1   
$EndComp
$Comp
L R R23
U 1 1 5B241B38
P 6500 3300
F 0 "R23" V 6580 3300 50  0000 C CNN
F 1 "270" V 6500 3300 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 6430 3300 50  0001 C CNN
F 3 "" H 6500 3300 50  0001 C CNN
	1    6500 3300
	-1   0    0    1   
$EndComp
Text HLabel 2250 3300 0    50   Input ~ 0
VBUS
Text HLabel 2250 3450 0    50   Input ~ 0
DP
Text HLabel 2250 3650 0    50   Input ~ 0
DM
Text HLabel 2250 3800 0    50   Input ~ 0
USBGND
$Comp
L L L3
U 1 1 5B242C2B
P 3350 3300
F 0 "L3" H 3250 3350 50  0000 C CNN
F 1 "L" H 3450 3350 50  0000 C CNN
F 2 "Inductors_SMD:L_0805_HandSoldering" V 3350 3300 50  0001 C CNN
F 3 "" V 3350 3300 50  0001 C CNN
F 4 "Type" H 3350 3450 50  0000 C CNN "Type"
	1    3350 3300
	1    0    0    -1  
$EndComp
$Comp
L C C19
U 1 1 5B2438D0
P 3700 3900
F 0 "C19" H 3725 4000 50  0000 L CNN
F 1 "100n" H 3725 3800 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 3738 3750 50  0001 C CNN
F 3 "" H 3700 3900 50  0001 C CNN
	1    3700 3900
	1    0    0    -1  
$EndComp
$Comp
L C_TAN C20
U 1 1 5B243914
P 3950 3900
F 0 "C20" H 3975 4000 50  0000 L CNN
F 1 "4u7" H 3975 3800 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 3988 3750 50  0001 C CNN
F 3 "" H 3950 3900 50  0001 C CNN
	1    3950 3900
	1    0    0    -1  
$EndComp
Text Label 5900 3700 0    50   ~ 0
~TXLED
Text Label 5900 3600 0    50   ~ 0
~RXLED
Wire Wire Line
	4250 3850 4600 3850
Wire Wire Line
	4500 3850 4500 4050
Wire Wire Line
	4500 4050 4600 4050
Connection ~ 4500 3850
Wire Wire Line
	4600 4250 4550 4250
Wire Wire Line
	4550 4250 4550 4650
Wire Wire Line
	4250 4250 4250 4350
Wire Wire Line
	3950 4350 4550 4350
Wire Wire Line
	4250 3950 4250 3850
Wire Wire Line
	4600 3500 4400 3500
Wire Wire Line
	4400 3500 4400 3450
Wire Wire Line
	4400 3450 3500 3450
Wire Wire Line
	3500 3650 4400 3650
Wire Wire Line
	4400 3650 4400 3600
Wire Wire Line
	4400 3600 4600 3600
Wire Wire Line
	6500 3050 6500 3150
Wire Wire Line
	6150 3150 6150 3050
Wire Wire Line
	6150 3600 6150 3450
Wire Wire Line
	5800 3600 6150 3600
Wire Wire Line
	6500 3700 6500 3450
Wire Wire Line
	5800 3700 6500 3700
Wire Wire Line
	6150 2750 6150 2600
Wire Wire Line
	4400 2600 6900 2600
Wire Wire Line
	6500 2600 6500 2750
Wire Wire Line
	3500 3300 4600 3300
Wire Wire Line
	2250 3300 3200 3300
Wire Wire Line
	2250 3450 3200 3450
Wire Wire Line
	2800 3750 2800 3450
Connection ~ 2800 3450
Wire Wire Line
	2250 3650 3200 3650
Wire Wire Line
	3050 3650 3050 3750
Connection ~ 3050 3650
Wire Wire Line
	2250 3800 2300 3800
Wire Wire Line
	2300 3800 2300 4150
Wire Wire Line
	2300 4150 3950 4150
Wire Wire Line
	3950 4050 3950 4350
Connection ~ 4250 4350
Wire Wire Line
	3050 4150 3050 4050
Connection ~ 3050 4150
Wire Wire Line
	2800 4150 2800 4050
Connection ~ 2800 4150
Wire Wire Line
	2550 4150 2550 4050
Connection ~ 2550 4150
Wire Wire Line
	2550 3750 2550 3300
Connection ~ 2550 3300
Wire Wire Line
	3950 3750 3950 3300
Connection ~ 3950 3300
Wire Wire Line
	3700 3750 3700 3300
Connection ~ 3700 3300
Wire Wire Line
	3700 4050 3700 4150
Connection ~ 3700 4150
Connection ~ 3950 4150
Wire Wire Line
	5900 4250 5900 4650
Wire Wire Line
	5800 4250 7400 4250
Connection ~ 4550 4350
Wire Wire Line
	4400 2600 4400 3300
Connection ~ 4400 3300
Connection ~ 6150 2600
Wire Wire Line
	5800 4000 7400 4000
Wire Wire Line
	5800 4100 7400 4100
Text HLabel 7400 4000 2    50   Input ~ 0
TXD
Text HLabel 7400 4100 2    50   Input ~ 0
RXD
Text HLabel 7400 4250 2    50   Input ~ 0
GND
Connection ~ 6500 2600
Connection ~ 5900 4250
Wire Wire Line
	4600 3950 4550 3950
Wire Wire Line
	4550 3950 4550 3850
Connection ~ 4550 3850
$Comp
L JUMPER JP6
U 1 1 5B1FEB3F
P 5150 4650
F 0 "JP6" H 5150 4700 50  0000 C CNN
F 1 "JUMPER" H 5200 4550 50  0001 C CNN
F 2 "FootprintLib:SOLDER_JUMPER" H 5150 4650 50  0001 C CNN
F 3 "" H 5150 4650 50  0001 C CNN
	1    5150 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 4650 5050 4650
Wire Wire Line
	5900 4650 5250 4650
Text Notes 900  1200 0    100  ~ 0
SchematicLib is required for this schematic. \nPlease write to siecinski.kamil@gmail.com if you need access to this library. \nIt is totally free.
Text HLabel 7400 3850 2    50   Input ~ 0
VZAS
Wire Wire Line
	7400 3850 6900 3850
Wire Wire Line
	6900 3850 6900 2600
$EndSCHEMATC
