EESchema Schematic File Version 2
LIBS:SchematicLib
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:MainControlUnit-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ISO154 U9
U 1 1 5B2B8F45
P 5500 3850
AR Path="/5B2B69E3/5B2B8F45" Ref="U9"  Part="1" 
AR Path="/5B30738C/5B2B8F45" Ref="U9"  Part="1" 
F 0 "U9" H 5200 4200 50  0000 C CNN
F 1 "ISO154" H 5700 4200 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 5500 3500 50  0001 C CIN
F 3 "" H 5550 3850 50  0001 C CNN
	1    5500 3850
	1    0    0    -1  
$EndComp
$Comp
L C C31
U 1 1 5B2B9205
P 6650 3850
AR Path="/5B2B69E3/5B2B9205" Ref="C31"  Part="1" 
AR Path="/5B30738C/5B2B9205" Ref="C31"  Part="1" 
F 0 "C31" H 6675 3950 50  0000 L CNN
F 1 "100n" H 6675 3750 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 6688 3700 50  0001 C CNN
F 3 "" H 6650 3850 50  0001 C CNN
	1    6650 3850
	1    0    0    -1  
$EndComp
$Comp
L C C30
U 1 1 5B2B9259
P 4400 3850
AR Path="/5B2B69E3/5B2B9259" Ref="C30"  Part="1" 
AR Path="/5B30738C/5B2B9259" Ref="C30"  Part="1" 
F 0 "C30" H 4425 3950 50  0000 L CNN
F 1 "100n" H 4425 3750 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4438 3700 50  0001 C CNN
F 3 "" H 4400 3850 50  0001 C CNN
	1    4400 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3700 4400 3650
Wire Wire Line
	3550 3650 5050 3650
Wire Wire Line
	3550 4050 5050 4050
Wire Wire Line
	4400 4050 4400 4000
Wire Wire Line
	5950 4050 7200 4050
Wire Wire Line
	6650 4050 6650 4000
Wire Wire Line
	5950 3650 7200 3650
Wire Wire Line
	6650 3650 6650 3700
Wire Wire Line
	5950 3800 6300 3800
Wire Wire Line
	5950 3900 6300 3900
Wire Wire Line
	5050 3800 4750 3800
Wire Wire Line
	5050 3900 4750 3900
Text Label 4750 3800 0    50   ~ 0
SDA1
Text Label 4750 3900 0    50   ~ 0
SCL1
Text Label 6300 3800 2    50   ~ 0
SDA2
Text Label 6300 3900 2    50   ~ 0
SCL2
Text HLabel 3550 3650 0    50   Input ~ 0
VCC1
Text HLabel 3550 4050 0    50   Input ~ 0
GND1
Text HLabel 3550 3800 0    50   Input ~ 0
SDA
Text HLabel 3550 3900 0    50   Input ~ 0
SCL
Wire Wire Line
	3550 3800 3750 3800
Wire Wire Line
	3550 3900 3750 3900
Text Label 3750 3800 2    50   ~ 0
SDA1
Text Label 3750 3900 2    50   ~ 0
SCL1
Connection ~ 4400 3650
Connection ~ 4400 4050
Text HLabel 7200 3650 2    50   Input ~ 0
VCC2
Text HLabel 7200 4050 2    50   Input ~ 0
GND2
Text HLabel 7200 3800 2    50   Input ~ 0
ISO_SDA
Text HLabel 7200 3900 2    50   Input ~ 0
ISO_SCL
Connection ~ 6650 3650
Connection ~ 6650 4050
Wire Wire Line
	7200 3800 6950 3800
Wire Wire Line
	7200 3900 6950 3900
Text Label 6950 3800 0    50   ~ 0
SDA2
Text Label 6950 3900 0    50   ~ 0
SCL2
$EndSCHEMATC
