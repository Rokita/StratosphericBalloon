EESchema Schematic File Version 2
LIBS:SchematicLib
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:MainControlUnit-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 11 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ISO7221C U10
U 1 1 5B30E7FC
P 5300 3500
F 0 "U10" H 5300 3925 50  0000 C CNN
F 1 "ISO7221C" H 5300 3850 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 5300 3150 50  0001 C CIN
F 3 "" H 5300 3500 50  0001 C CNN
	1    5300 3500
	1    0    0    -1  
$EndComp
$Comp
L C C33
U 1 1 5B30E7FD
P 4300 3500
F 0 "C33" H 4325 3600 50  0000 L CNN
F 1 "100n" H 4325 3400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4338 3350 50  0001 C CNN
F 3 "" H 4300 3500 50  0001 C CNN
	1    4300 3500
	1    0    0    -1  
$EndComp
$Comp
L C C34
U 1 1 5B30E7FE
P 6300 3500
F 0 "C34" H 6325 3600 50  0000 L CNN
F 1 "100n" H 6325 3400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 6338 3350 50  0001 C CNN
F 3 "" H 6300 3500 50  0001 C CNN
	1    6300 3500
	1    0    0    -1  
$EndComp
Text HLabel 3650 3300 0    50   Input ~ 0
Vcc1
Text HLabel 3650 3550 0    50   Input ~ 0
TXD_IN
Text HLabel 7050 3450 2    50   Input ~ 0
RXD_IN
Text HLabel 3650 3700 0    50   Input ~ 0
GND1
Wire Wire Line
	3650 3700 4900 3700
Wire Wire Line
	4300 3700 4300 3650
Wire Wire Line
	4300 3350 4300 3300
Wire Wire Line
	3650 3300 4900 3300
Wire Wire Line
	5700 3300 7050 3300
Wire Wire Line
	6300 3300 6300 3350
Wire Wire Line
	5700 3700 7050 3700
Wire Wire Line
	6300 3700 6300 3650
Connection ~ 4300 3300
Connection ~ 4300 3700
Wire Wire Line
	3650 3550 4050 3550
Wire Wire Line
	3650 3450 4050 3450
Wire Wire Line
	4900 3450 4550 3450
Wire Wire Line
	4550 3550 4900 3550
Text Label 4050 3550 2    50   ~ 0
TXD_IN
Text Label 4550 3550 0    50   ~ 0
TXD_IN
Text Label 6050 3450 2    50   ~ 0
RXD_IN
Text HLabel 7050 3550 2    50   Input ~ 0
TXD_OUT
Text HLabel 3650 3450 0    50   Input ~ 0
RXD_OUT
Wire Wire Line
	7050 3550 6700 3550
Wire Wire Line
	7050 3450 6700 3450
Text Label 6700 3550 0    50   ~ 0
TXD_OUT
Wire Wire Line
	6050 3450 5700 3450
Wire Wire Line
	6050 3550 5700 3550
Text Label 6050 3550 2    50   ~ 0
TXD_OUT
Text Label 4550 3450 0    50   ~ 0
RXD_OUT
Text HLabel 7050 3300 2    50   Input ~ 0
Vcc2
Text HLabel 7050 3700 2    50   Input ~ 0
GND2
Connection ~ 6300 3300
Connection ~ 6300 3700
Text Label 4050 3450 2    50   ~ 0
RXD_OUT
Text Label 6700 3450 0    50   ~ 0
RXD_IN
Text Notes 1050 1200 0    100  ~ 0
SchematicLib is required for this schematic. \nPlease write to siecinski.kamil@gmail.com if you need access to this library. \nIt is totally free.
$EndSCHEMATC
