EESchema Schematic File Version 2
LIBS:SchematicLib
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:MainControlUnit-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Battery_Cell BT1
U 1 1 5B29D753
P 6700 3650
F 0 "BT1" H 6800 3750 50  0000 L CNN
F 1 "Battery_Cell" H 6800 3650 50  0000 L CNN
F 2 "FootprintLib:Battery_CR1220" V 6700 3710 50  0001 C CNN
F 3 "" V 6700 3710 50  0001 C CNN
	1    6700 3650
	1    0    0    -1  
$EndComp
$Comp
L DS1338 U8
U 1 1 5B29DFD9
P 5800 3350
F 0 "U8" H 5550 3650 50  0000 C CNN
F 1 "DS1338" H 5950 3650 50  0000 C CNN
F 2 "Housings_SOIC:SO-8_5.3x6.2mm_Pitch1.27mm" H 5800 3400 50  0001 C CNN
F 3 "" H 5800 3400 50  0001 C CNN
F 4 "SOIC8" H 5800 3050 60  0001 C CNN "Package"
	1    5800 3350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6700 3400 6700 3450
Wire Wire Line
	6200 3400 6700 3400
Wire Wire Line
	6700 3800 6700 3750
Wire Wire Line
	3250 3800 6700 3800
Wire Wire Line
	6250 3800 6250 3500
Wire Wire Line
	6250 3500 6200 3500
$Comp
L Crystal Y1
U 1 1 5B29E091
P 6450 3050
F 0 "Y1" H 6450 3200 50  0000 C CNN
F 1 "Crystal" H 6450 2900 50  0000 C CNN
F 2 "FootprintLib:CrystalOscillator_3.2x1.5_SMD" H 6450 3050 50  0001 C CNN
F 3 "" H 6450 3050 50  0001 C CNN
	1    6450 3050
	1    0    0    1   
$EndComp
$Comp
L C C29
U 1 1 5B29E0CC
P 6450 3600
F 0 "C29" H 6475 3700 50  0000 L CNN
F 1 "100n" H 6475 3500 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 6488 3450 50  0001 C CNN
F 3 "" H 6450 3600 50  0001 C CNN
	1    6450 3600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6450 3450 6450 3400
Connection ~ 6450 3400
Wire Wire Line
	6450 3750 6450 3800
Connection ~ 6450 3800
$Comp
L C C28
U 1 1 5B29E1B8
P 4150 3350
F 0 "C28" H 4175 3450 50  0000 L CNN
F 1 "100n" H 4175 3250 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4188 3200 50  0001 C CNN
F 3 "" H 4150 3350 50  0001 C CNN
	1    4150 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 3050 6650 3050
Wire Wire Line
	6650 3050 6650 3300
Wire Wire Line
	6650 3300 6200 3300
Wire Wire Line
	6200 3200 6250 3200
Wire Wire Line
	6250 3200 6250 3050
Wire Wire Line
	6250 3050 6300 3050
$Comp
L R R34
U 1 1 5B29E58B
P 4900 3100
F 0 "R34" V 4980 3100 50  0000 C CNN
F 1 "4K7" V 4900 3100 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4830 3100 50  0001 C CNN
F 3 "" H 4900 3100 50  0001 C CNN
	1    4900 3100
	-1   0    0    -1  
$EndComp
$Comp
L R R33
U 1 1 5B29E621
P 4700 3100
F 0 "R33" V 4780 3100 50  0000 C CNN
F 1 "4K7" V 4700 3100 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4630 3100 50  0001 C CNN
F 3 "" H 4700 3100 50  0001 C CNN
	1    4700 3100
	-1   0    0    -1  
$EndComp
$Comp
L R R32
U 1 1 5B29E64A
P 4500 3100
F 0 "R32" V 4580 3100 50  0000 C CNN
F 1 "4K7" V 4500 3100 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4430 3100 50  0001 C CNN
F 3 "" H 4500 3100 50  0001 C CNN
	1    4500 3100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5400 3300 4900 3300
Wire Wire Line
	4900 3300 4900 3250
Wire Wire Line
	5400 3400 4700 3400
Wire Wire Line
	4700 3400 4700 3250
Wire Wire Line
	5400 3500 4500 3500
Wire Wire Line
	4500 3500 4500 3250
Wire Wire Line
	5400 3200 5200 3200
Wire Wire Line
	5200 3200 5200 2900
Wire Wire Line
	5200 2900 2650 2900
Wire Wire Line
	4500 2950 4500 2900
Connection ~ 4500 2900
Wire Wire Line
	4700 2950 4700 2900
Connection ~ 4700 2900
Wire Wire Line
	4900 2950 4900 2900
Connection ~ 4900 2900
Wire Wire Line
	4150 2900 4150 3200
Wire Wire Line
	4150 3500 4150 3800
Connection ~ 6250 3800
Connection ~ 4150 2900
Wire Wire Line
	3250 3050 3250 3800
Wire Wire Line
	2650 3050 3250 3050
Connection ~ 4150 3800
Text Label 5000 3300 0    60   ~ 0
SWO
Text Label 5000 3400 0    60   ~ 0
SCL
Text Label 5000 3500 0    60   ~ 0
SDA
Wire Wire Line
	3050 3300 2650 3300
Wire Wire Line
	3050 3400 2650 3400
Wire Wire Line
	3050 3500 2650 3500
Text Label 3050 3300 2    60   ~ 0
SWO
Text Label 3050 3400 2    60   ~ 0
SCL
Text Label 3050 3500 2    60   ~ 0
SDA
Text HLabel 2650 2900 0    60   Input ~ 0
VCC
Text HLabel 2650 3050 0    60   Input ~ 0
GND
Text HLabel 2650 3300 0    60   Input ~ 0
SWO
Text HLabel 2650 3400 0    60   Input ~ 0
SCL
Text HLabel 2650 3500 0    60   Input ~ 0
SDA
Text Label 3050 2900 2    60   ~ 0
VCC
Text Label 3050 3050 2    60   ~ 0
GND
$Comp
L C_TAN C27
U 1 1 5B29FCBC
P 3750 3350
F 0 "C27" H 3775 3450 50  0000 L CNN
F 1 "4u7" H 3775 3250 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 3788 3200 50  0001 C CNN
F 3 "" H 3750 3350 50  0001 C CNN
	1    3750 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 3200 3750 2900
Connection ~ 3750 2900
Wire Wire Line
	3750 3500 3750 3800
Connection ~ 3750 3800
$EndSCHEMATC
