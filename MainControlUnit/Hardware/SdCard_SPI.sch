EESchema Schematic File Version 2
LIBS:SchematicLib
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:MainControlUnit-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 2550 3250 0    60   Input ~ 0
Vcc
Text HLabel 2550 4150 0    60   Input ~ 0
GND
$Comp
L Micro_SD_Card_Det J5
U 1 1 5B282C42
P 7000 4050
F 0 "J5" H 6350 4750 50  0000 C CNN
F 1 "Micro_SD_Card_Det" H 7650 4750 50  0000 R CNN
F 2 "FootprintLib:MOLEX_5027740891_SD_CARD" H 9050 4750 50  0001 C CNN
F 3 "" H 7000 4150 50  0001 C CNN
	1    7000 4050
	1    0    0    -1  
$EndComp
Text Label 2850 3250 2    60   ~ 0
Vcc
Text Label 5800 4150 0    60   ~ 0
GND
Text Label 5800 3950 0    60   ~ 0
Vcc
Text Label 2850 4150 2    60   ~ 0
GND
$Comp
L C_TAN C25
U 1 1 5B282DCB
P 2950 4900
F 0 "C25" H 2975 5000 50  0000 L CNN
F 1 "47u" H 2975 4800 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2988 4750 50  0001 C CNN
F 3 "" H 2950 4900 50  0001 C CNN
	1    2950 4900
	1    0    0    -1  
$EndComp
$Comp
L C C24
U 1 1 5B282DF1
P 2600 4900
F 0 "C24" H 2625 5000 50  0000 L CNN
F 1 "100n" H 2625 4800 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2638 4750 50  0001 C CNN
F 3 "" H 2600 4900 50  0001 C CNN
	1    2600 4900
	1    0    0    -1  
$EndComp
Text HLabel 2550 3500 0    60   Input ~ 0
MISO
Text HLabel 2550 3600 0    60   Input ~ 0
MOSI
Text HLabel 2550 3700 0    60   Input ~ 0
SCK
Text HLabel 2550 3800 0    60   Input ~ 0
~CS
Text Label 2850 3500 2    60   ~ 0
MISO
Text Label 2850 3600 2    60   ~ 0
MOSI
Text Label 2850 3700 2    60   ~ 0
SCK
Text Label 2850 3800 2    60   ~ 0
~CS
Text Label 5800 4250 0    60   ~ 0
MISO
Text Label 5800 3850 0    60   ~ 0
MOSI
Text Label 5800 4050 0    60   ~ 0
SCK
Text Label 5800 3750 0    60   ~ 0
~CS
Text Label 2350 4700 0    60   ~ 0
Vcc
Text Label 2350 5100 0    60   ~ 0
GND
Text HLabel 2550 3900 0    60   Input ~ 0
~DET
Text Label 2850 3900 2    60   ~ 0
~DET
Text Label 5800 4450 0    60   ~ 0
~DET
Text Label 5800 4550 0    60   ~ 0
GND
Text Label 8100 4550 2    60   ~ 0
GND
$Comp
L R R24
U 1 1 5B283DCF
P 4350 3450
F 0 "R24" V 4430 3450 50  0000 C CNN
F 1 "10K" V 4350 3450 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4280 3450 50  0001 C CNN
F 3 "" H 4350 3450 50  0001 C CNN
	1    4350 3450
	1    0    0    -1  
$EndComp
$Comp
L R R26
U 1 1 5B2892C4
P 4550 3450
F 0 "R26" V 4630 3450 50  0000 C CNN
F 1 "10K" V 4550 3450 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4480 3450 50  0001 C CNN
F 3 "" H 4550 3450 50  0001 C CNN
	1    4550 3450
	1    0    0    -1  
$EndComp
$Comp
L R R27
U 1 1 5B2892E7
P 4750 3450
F 0 "R27" V 4830 3450 50  0000 C CNN
F 1 "10K" V 4750 3450 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4680 3450 50  0001 C CNN
F 3 "" H 4750 3450 50  0001 C CNN
	1    4750 3450
	1    0    0    -1  
$EndComp
$Comp
L R R28
U 1 1 5B289311
P 4950 3450
F 0 "R28" V 5030 3450 50  0000 C CNN
F 1 "10K" V 4950 3450 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4880 3450 50  0001 C CNN
F 3 "" H 4950 3450 50  0001 C CNN
	1    4950 3450
	1    0    0    -1  
$EndComp
$Comp
L R R31
U 1 1 5B28933A
P 5550 3450
F 0 "R31" V 5630 3450 50  0000 C CNN
F 1 "10K" V 5550 3450 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 5480 3450 50  0001 C CNN
F 3 "" H 5550 3450 50  0001 C CNN
	1    5550 3450
	1    0    0    -1  
$EndComp
$Comp
L R R30
U 1 1 5B289394
P 5350 3450
F 0 "R30" V 5430 3450 50  0000 C CNN
F 1 "10K" V 5350 3450 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 5280 3450 50  0001 C CNN
F 3 "" H 5350 3450 50  0001 C CNN
	1    5350 3450
	1    0    0    -1  
$EndComp
$Comp
L R R29
U 1 1 5B289B36
P 5150 3450
F 0 "R29" V 5230 3450 50  0000 C CNN
F 1 "10K" V 5150 3450 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 5080 3450 50  0001 C CNN
F 3 "" H 5150 3450 50  0001 C CNN
	1    5150 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 3750 6100 3750
Wire Wire Line
	4950 4050 6100 4050
Wire Wire Line
	5150 3850 6100 3850
Wire Wire Line
	4750 4250 6100 4250
Wire Wire Line
	2550 3250 2850 3250
Wire Wire Line
	2550 4150 2850 4150
Wire Wire Line
	2550 3500 2850 3500
Wire Wire Line
	2550 3600 2850 3600
Wire Wire Line
	2550 3700 2850 3700
Wire Wire Line
	2550 3800 2850 3800
Wire Wire Line
	5800 4150 6100 4150
Wire Wire Line
	6100 3950 5800 3950
Wire Wire Line
	2350 4700 2950 4700
Wire Wire Line
	2350 5100 2950 5100
Wire Wire Line
	2950 4700 2950 4750
Wire Wire Line
	2600 4750 2600 4700
Connection ~ 2600 4700
Wire Wire Line
	2600 5100 2600 5050
Wire Wire Line
	2950 5100 2950 5050
Connection ~ 2600 5100
Wire Wire Line
	2550 3900 2850 3900
Wire Wire Line
	4350 4450 6100 4450
Wire Wire Line
	6100 4550 5800 4550
Wire Wire Line
	7800 4550 8100 4550
Wire Wire Line
	6100 3650 5550 3650
Wire Wire Line
	5550 3650 5550 3600
Wire Wire Line
	5350 3750 5350 3600
Wire Wire Line
	5150 3850 5150 3600
Wire Wire Line
	4950 4050 4950 3600
Wire Wire Line
	4750 4250 4750 3600
Wire Wire Line
	6100 4350 4550 4350
Wire Wire Line
	4550 4350 4550 3600
Wire Wire Line
	4350 3600 4350 4500
Wire Wire Line
	5550 3250 5550 3300
Wire Wire Line
	4150 3250 5550 3250
Wire Wire Line
	4350 3300 4350 3250
Connection ~ 4350 3250
Wire Wire Line
	4550 3300 4550 3250
Connection ~ 4550 3250
Wire Wire Line
	4750 3300 4750 3250
Connection ~ 4750 3250
Wire Wire Line
	4950 3300 4950 3250
Connection ~ 4950 3250
Wire Wire Line
	5150 3300 5150 3250
Connection ~ 5150 3250
Wire Wire Line
	5350 3300 5350 3250
Wire Wire Line
	5350 3250 5300 3250
Connection ~ 5300 3250
Text Label 4150 3250 0    60   ~ 0
Vcc
$Comp
L R R25
U 1 1 5B28A523
P 4350 4650
F 0 "R25" V 4430 4650 50  0000 C CNN
F 1 "100" V 4350 4650 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4280 4650 50  0001 C CNN
F 3 "" H 4350 4650 50  0001 C CNN
	1    4350 4650
	1    0    0    -1  
$EndComp
Connection ~ 4350 4450
$Comp
L C C26
U 1 1 5B28A636
P 4350 5050
F 0 "C26" H 4375 5150 50  0000 L CNN
F 1 "100n" H 4375 4950 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4388 4900 50  0001 C CNN
F 3 "" H 4350 5050 50  0001 C CNN
	1    4350 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 4900 4350 4800
Text Label 4050 5300 0    60   ~ 0
GND
Wire Wire Line
	4050 5300 4350 5300
Wire Wire Line
	4350 5300 4350 5200
Text Notes 6300 4900 0    60   ~ 0
MOLEX \nMX-502774-0891
$EndSCHEMATC
