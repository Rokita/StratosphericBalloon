EESchema Schematic File Version 2
LIBS:SchematicLib
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:PowerControlUnit-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Con_1x2 J1
U 1 1 5B018568
P 1850 2500
F 0 "J1" H 1850 2700 50  0000 C CNN
F 1 "Con_1x2" H 1850 2400 50  0000 C CNN
F 2 "FootprintLib:AMASS-XT30-2P-THT" H 1850 2500 50  0001 C CNN
F 3 "" H 1850 2500 50  0001 C CNN
	1    1850 2500
	-1   0    0    1   
$EndComp
$Comp
L Con_1x2 J4
U 1 1 5B01ADF8
P 8900 2200
F 0 "J4" H 8900 2400 50  0000 C CNN
F 1 "Con_1x2" H 8900 2100 50  0000 C CNN
F 2 "Connectors_JST:JST_EH_B02B-EH-A_02x2.50mm_Straight" H 8900 2200 50  0001 C CNN
F 3 "" H 8900 2200 50  0001 C CNN
	1    8900 2200
	1    0    0    -1  
$EndComp
$Comp
L Con_1x2 J5
U 1 1 5B01B33B
P 8900 2650
F 0 "J5" H 8900 2850 50  0000 C CNN
F 1 "Con_1x2" H 8900 2550 50  0000 C CNN
F 2 "Connectors_JST:JST_EH_B02B-EH-A_02x2.50mm_Straight" H 8900 2650 50  0001 C CNN
F 3 "" H 8900 2650 50  0001 C CNN
	1    8900 2650
	1    0    0    -1  
$EndComp
$Comp
L Con_1x2 J6
U 1 1 5B01B3C7
P 8900 3100
F 0 "J6" H 8900 3300 50  0000 C CNN
F 1 "Con_1x2" H 8900 3000 50  0000 C CNN
F 2 "Connectors_JST:JST_EH_B02B-EH-A_02x2.50mm_Straight" H 8900 3100 50  0001 C CNN
F 3 "" H 8900 3100 50  0001 C CNN
	1    8900 3100
	1    0    0    -1  
$EndComp
$Sheet
S 2950 1800 4850 1600
U 5AD5F897
F0 "Payload Power Control Unit" 60
F1 "PayloadPCU.sch" 60
F2 "BAT+" I L 2950 2500 50 
F3 "BAT-" I L 2950 2600 50 
F4 "3V7_GND" I R 7800 2650 50 
F5 "3V3_GND" I R 7800 3100 50 
F6 "BATT" I R 7800 2100 50 
F7 "BATT_GND" I R 7800 2200 50 
F8 "P3V7" I R 7800 2550 60 
F9 "P3V3" I R 7800 3000 60 
$EndSheet
Wire Wire Line
	2050 2500 2950 2500
Wire Wire Line
	2050 2600 2950 2600
Wire Wire Line
	7800 2550 8700 2550
Wire Wire Line
	7800 2650 8700 2650
Wire Wire Line
	7800 3000 8700 3000
Wire Wire Line
	7800 3100 8700 3100
Wire Wire Line
	7800 2200 8700 2200
Wire Wire Line
	7800 2100 8700 2100
$Comp
L Con_1x2 J7
U 1 1 5B0B3985
P 1850 5400
F 0 "J7" H 1850 5600 50  0000 C CNN
F 1 "Con_1x2" H 1850 5300 50  0000 C CNN
F 2 "FootprintLib:AMASS-XT30-2P-THT" H 1850 5400 50  0001 C CNN
F 3 "" H 1850 5400 50  0001 C CNN
	1    1850 5400
	-1   0    0    1   
$EndComp
Wire Wire Line
	2050 5400 2950 5400
Wire Wire Line
	2050 5500 2950 5500
$Comp
L Con_1x3 J8
U 1 1 5B0C4BA7
P 8900 5600
F 0 "J8" H 8900 5800 50  0000 C CNN
F 1 "Con_1x3" H 8900 5400 50  0000 C CNN
F 2 "Connectors_JST:JST_EH_B03B-EH-A_03x2.50mm_Straight" H 8900 5600 50  0001 C CNN
F 3 "" H 8900 5600 50  0001 C CNN
	1    8900 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 5500 8300 5500
Wire Wire Line
	8300 5500 8300 5350
Wire Wire Line
	8300 5350 7800 5350
Wire Wire Line
	7800 5600 8700 5600
Wire Wire Line
	8700 5700 8300 5700
Wire Wire Line
	8300 5700 8300 5850
Wire Wire Line
	8300 5850 7800 5850
$Sheet
S 2950 4650 4850 1550
U 5AD5F8B8
F0 "Gondola Power Control Unit" 60
F1 "GondolaPCU.sch" 60
F2 "Gondola_BAT+" I L 2950 5400 50 
F3 "Gondola_BAT-" I L 2950 5500 50 
F4 "+5V" I R 7800 5600 50 
F5 "GNDD" I R 7800 5850 50 
F6 "Gondola_+BATT" I R 7800 5350 50 
$EndSheet
$EndSCHEMATC
